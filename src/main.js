import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import 'uno.css';
import 'ant-design-vue/es/message/style/css'; //vite只能用 ant-design-vue/es 而非 ant-design-vue/lib

createApp(App).use(router).mount('#app');
