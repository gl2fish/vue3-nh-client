import { request } from '@/utils/service.js';

export function getTabs() {
  return request({
    url: `center/client/tabs`,
    method: 'get',
  });
}

export function getList(type, keyword = '') {
  if (type) {
    return request({
      url: `center/client/list?type=${type}&keyword=${keyword}`,
      method: 'get',
    });
  }
  return request({
    url: `center/client/list?keyword=${keyword}`,
    method: 'get',
  });
}
